/* mergesort.cxx 
 * Copyright 2014 Miquel Pericas and Tokyo Institute of Technology
 * This program is licensed under the terms of the New BSD License.
 * See LICENSE for full details
 * 
 * mergesort.cxx: simple serial/parallel example using LOI/KRD based on a mergesort algorithm
 *
 * This program implements a divide and serial/parallel conquer sort. The leaf nodes sort using qsort() and
 * the inner nodes use a mergesort 
 *
 * NOTE: this is NOT the same code as the code identified as cilksort in 
 * "Scalable Analysis of Multicore Data Reuse and Sharing", ICS 2014
 * */

#if TBB 
#include <tbb/task_group.h>
#include "tbb/task_scheduler_init.h"
#include "tbb/tbb.h"
using namespace tbb; // for task_scheduler_init
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef TBB
#endif

#ifdef DO_LOI
#include "loi.h"

enum kernels{
    MERGE = 0, 
    QSORT = 1, // this is libc qsort(), it does not necessarily implement quicksort
    };

/* this structure describes the relationship between phases and kernels in the application */ 
struct loi_kernel_info sort_kernels = {
        2,              // 2 kernels in total
        1,              // 1 phase
        {"Merge_Core", "QSort_Core"}, // Name of the two kernels
        {"Sort"},                     // Name of the phase
        {(1<<MERGE | 1<<QSORT)}, // both kernels belong to the same phase [0]
                                 // if additional phases exist, just add an additional bitmask after a comma 
        };

#endif


typedef long int Number;
int do_merge(Number *start1, int size1, Number *start2, int size2);

// array merging, w/ profiling (DO_LOI) and tracing (DO_KRD)
void merge(Number *start1, int size1, Number *start2, int size2)
{
#if DO_LOI
      kernel_profile_start();
#endif

      do_merge(start1, size1, start2, size2);

#if DO_LOI
      kernel_profile_stop(MERGE);
#if DO_KRD
      kernel_trace1(start1, KWRITE(size1 + size2)*sizeof(Number));
#endif
#endif

      return;
}

// compare two numbers: if the first is smaller than the second then the comparison should be negative
static int numcompare(const void *elem1, const void *elem2) { 
    return (int) *((Number *) elem1) - *((Number *) elem2); 
    } 

// Numbers are merged from sequential chunks
int do_merge(Number *start1, int size1, Number *start2, int size2)
{
    Number *ptr1, *ptr2;

    // 1. Allocate temporal space to store the temporal merge
    //  Size is (end1 - start1 + 1) + (end2 - start2 + 1)
    int arraysize = size1 + size2;
    Number *temp = (Number *) malloc(sizeof(Number) * (arraysize));
    Number *mergeptr = temp;

    // 2. start merge with 2-indexed loop
    for(ptr1 = start1, ptr2 = start2; 
        ptr1 != (start1 + size1) || ptr2 != (start2 + size2) ; ){ 
            if(ptr1 == start1 + size1){  
                *mergeptr = *ptr2; 
                ptr2++; mergeptr++;
                }
            else if(ptr2 == start2 + size2){
                *mergeptr = *ptr1;
                ptr1++; mergeptr++;
                }
            else if(numcompare(ptr1, ptr2) < 0){   
                *mergeptr = *ptr1;           
                ptr1++; mergeptr++;
                }
            else {
                *mergeptr = *ptr2;          
                ptr2++; mergeptr++;
                }
    }  // we are done

    // 3. copy back merged items into original vector
    //  the merges are always contiguous, so the output vector should look like [start1][-----][end2-1]
    for(int i = 0; i < arraysize; i++)
        start1[i] = temp[i];

    // 4. free allocation
    free(temp);

    // This implementation assumes that malloc is thread safe
    // This should be ok for any recent unix malloc implementation
    //
    // Note that the malloc has only local scope. For this reason we do not include it in the KRD trace.
    // This data is never reused (the area may be reused, though)
}

// leaf sorting, w/ profiling (DO_LOI) and tracing (DO_KRD) 
void myqsort(void *array, int nmemb, int size, int(*compar)(const void *, const void *))
{

#if DO_LOI
    kernel_profile_start();
#endif

    qsort(array, nmemb, size, compar);

#if DO_LOI
    kernel_profile_stop(QSORT);
#if DO_KRD
    kernel_trace1(array, KWRITE(nmemb * size));
#endif
#endif
}

// recursion depth is determined by the array size (threshold) below which we directly apply the qsort() method
#define THRESHOLD 1000

static int threshold = THRESHOLD;

int mergesort(Number *start_elem, int array_size) 
{
//  find the middle element and split into two halves
    int start_size = array_size / 2;
    Number *mid_elem = start_elem + start_size;
    int mid_size = array_size - start_size; 

//  if the array is large enough, then continue recursing
    if(array_size > threshold){
#if TBB // TBB tasks expressed with lambdas
    task_group tg;
    tg.run([=] { mergesort(start_elem, start_size); });
    tg.run([=] { mergesort(mid_elem, mid_size); });
    tg.wait();
#else   // sequential version
    mergesort(start_elem, start_size);
    mergesort(mid_elem, mid_size);
#endif
    }
//
//  otherwise sort directly
    else{
#if TBB // exploit some extra parallelism
    task_group tg;
    tg.run([=] { myqsort(start_elem, start_size, sizeof(Number), numcompare); });
    tg.run([=] { myqsort(mid_elem,   mid_size,   sizeof(Number), numcompare); });
    tg.wait();
#else // sequential version
    myqsort(start_elem, start_size, sizeof(Number), numcompare);
    myqsort(mid_elem,   mid_size,   sizeof(Number), numcompare); 
#endif
    }

//  Now merge and return to the parent function which will merge recursively until the root
    merge(start_elem, start_size, mid_elem, mid_size);
}

// print the array, but only up to 10 elements so that we do not clutter the output too much
// It is just for checking
void print_array(Number *array, int size)
{

  if((size > 10) || !size)  // do not print more than 10 entries
     size = 10;  

  for(int i = 0; i < size; i++)
    printf("Array[%d] = %ld\n", i, (long int) array[i]);
}

// make sure that the array is ordered. Otherwise print a failure message and exit
void check_ordered(Number *array, int size)
{
  for(int i = 1; i < size; i++)
    if(array[i] < array[i-1]){
        printf("Error: out of order sequence detected at entry %d\n", i);
        exit(1);
        }
}



int main(int argc, char **argv)
{
    Number *array; 
    int size = 100000;

    if(argc >= 2)
        size = atoi(argv[1]);  // size of the array
    if(argc >= 3) 
        threshold = atoi(argv[2]);  // size of the leaf tasks
    if(argc >= 4){
        printf("Usage: ./mergesort <array_size> <leaf_size>\n");
        exit(0);
        }

    printf("MergeSort of %d random integers with leaf size %d\n", size, threshold);

#ifdef TBB
    // initialize threads
    int tbb_nthreads;
    if(getenv("TBB_NTHREADS"))
        tbb_nthreads = atoi(getenv("TBB_NTHREADS"));
    else 
        tbb_nthreads = task_scheduler_init::automatic;
    task_scheduler_init init(tbb_nthreads);
#endif
#if DO_LOI
    loi_init(); // calc TSC freq and init data structures
    printf(" TSC frequency has been measured to be: %g Hz\n", (double) TSCFREQ);
#endif


#if DO_LOI && TBB
    int maxthr;
    if (tbb_nthreads > 0) 
        maxthr = tbb_nthreads;
    else
        maxthr =  task_scheduler_init::default_num_threads();
#else 
    int maxthr = 1;
#endif

     array = (Number *) malloc(size * sizeof(Number));

     // initialize the buffer with random values
    for(int i = 0; i < size; i++)
    array[i] = random();

    printf("\nInitial Array\n");
    print_array(array, size);
    // check_ordered(array, size);

#ifdef DO_LOI 
    phase_profile_start();
#endif

#if TBB
    task_group tg;

    // start mergesort
    tg.run([=] { mergesort(array, size); });
    tg.wait();
#else
    mergesort(array, size);
#endif
#ifdef DO_LOI
    phase_profile_stop(0); 
#endif

    check_ordered(array, size);
    printf("\nSorted Array\n");
    print_array(array, size);

#ifdef DO_LOI
#ifdef LOI_TIMING
    loi_statistics(&sort_kernels, maxthr);
#endif
#ifdef DO_KRD
    krd_save_traces();
#endif
#endif

    free(array);
    return 0;
}
