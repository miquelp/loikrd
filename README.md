This repository contains the source code for the LOI/KRD performance analysis tools.  
LOI/KRD consists of two small profiling and tracings tools known as LOI (low overhead instrumentation) and KRD (kernel reuse distance). 
LOI is a lightweight profiler that provides information on work idleness and work time inflation while attempting to minimize overheads. 
KRD is a tracing library built on top of LOI to study coherence-aware
reuse distances for parallel executions on shared memory multicore architectures.

These sources are published as a proof-of-concept and in the hope that they prove useful to researchers on
reuse distance analysis and developers interested in understanding the locality and performance
characteristics of their codes. This tool is a research project. Stability and ease-of-use were not the main
goals. However, in my experience, adapting a new code to LoI/KRD is fairly straightforward. Going from native
to profiling and histogram generation should take about an hour of time, assuming you are familiar with the
procedure.

This README file describes the structure of the directory tree and how to compile and use the toolchain. The
main tutorial is located in ./examples/TUTORIAL. If you are interested in the theory and applications of the
toolchain, the following two articles are recommended reads: 

  1.  The main publication describing KRD is: "Scalable Analysis of Multicore Data Sharing and Reuse", Miquel
  Pericàs, Kenjiro Taura and Satoshi Matsuoka, International Conference on Supercomputing (ICS-14), Munich, 2014

  2.  An earlier paper that describes an earlier version of the tool with application to the performance and
  locality analysis of task-parallel runtimes is: "Analysis of Data Reuse in Task-Parallel Runtimes", Miquel
  Pericàs, Abdelhalim Amer, Kenjiro Taura, Satoshi Matsuoka, Performance Modeling, Benchmarking and Simulation
  (PMBS'13), Denver, 2013



**Index**

1.  Licensing
2.  Description of Directory Hierarchy
3.  Compiling the toolchain
4.  Compiling the examples
5.  Understanding the output of LOI and KRD
6.  Acknowledgement

----

1.  Licensing
-------------

    The copyright holders of the code are Miquel Pericàs and the Tokyo Institute of Technology. The code is
    licensed under the terms of the New BSD License (three-clause). The details are described in the file LICENSE
    which can be found in the root node of this directory tree.


2.  Directory Hierarchy
-----------------------

After decompressing the archive or cloning the repository you should be left with the following structure:

```
miquel@:loi-pub> tree
.
├── examples
│   ├── Makefile
│   ├── mergesort.cxx
│   ├── TUTORIAL
│   └── runtime.defs.sample
├── krd_bench.c
├── krd.c
├── krd_common.c
├── krd_common.h
├── krd.h
├── krd_plot.gpl
├── krd_trace.c
├── LICENSE
├── loi.c
├── loi.h
├── loikrd.defs.sample
├── Makefile
└── README
``` 

The root folder contains the main files part of this project:

```
1)  Low-overhead instrumentation -> loi.c, loi.h
2)  KRD application component    -> krd_trace.c, krd_bench.c
3)  KRD processing toolchain     -> krd_trace.c
4)  KRD histogram generation     -> krd.c
5)  KRD common files             -> krd_common.c, krd_common.h, krd.h
6)  KRD gnuplot file             -> krd_plot.gpl
7)  Toolchain Makefile           -> Makefile, loikrd.defs.sample
8)  Licensing details            -> LICENSE
9)  This file                    -> README
```

The 'examples' directory contains a model code that exemplifies the usage of the LOI/KRD tools

```
1)  MergeSort example            -> mergesort.cxx
2)  Compilation                  -> Makefile, runtime.defs.sample
3)  README/Tutorial              -> TUTORIAL
```


3. Compiling the toolchain
--------------------------

'Makefile' controls the compilation of the LOI/KRD toolchain. It has two main targets: 'all' and 'clean'.
In order to use LOI/KRD, you need to first copy loikrd.defs.sample to loikrd.defs and edit it according to
your need and environment. Rules for editing are provided as part of the sample file. 

'all' will generate all the tools of the toolchain, namely:
```
  - accum_tool: tool for accumulating histograms
  - merge_tool: tool for merging RAW traces
  - intlv_tool: tool for interleaving RAW traces (rarely used)
  - krd_tool: tool for generating the histogram from an RWI trace
  - coherence_tool: tool to generate coherent RWI traces
  - partition_tool: tool to partition the trace for parallel processing
  - dump_tool:  tool to write a trace to the terminal. Mainly used for debugging
```

To see how to use these binaries, check the README/Tutorial in the examples directory

LoI/KRD has been tested on x86-64 machines running Linux kernel version >=2.6.32 and with GCC versions >=4.6.3.  Porting LoI/KRD to other platforms is hopefully not too difficult. In particular, the following platform-specific features are being used: 

Linux-specific Features:
```
- Linux stores the logical core ID in the IA32_TSC_AUX register which can be retrieved using the RDTSCP
  instruction. We make use of this feature to obtain fast indexes into the per core private trace and
  profiling memory. Linux also stores the NUMA id in this register but LoI/KRD currently does not make use of
  this information. 
- On platforms on which the RDTSCP instruction is not available, LOI/KRD can use the sched_getcpu() system
  call. sched_getcpu() is only available on Linux systems using kernel version 2.6.19 or higher.
```

x86-64 specific Features:
```
- RDTSC and RDTSCP instructions are used to get precise timing for events. In newer processors, the TSC
  (time-stamp counter) is kept synchronous across all cores. Furthermore, at reset time the different TSCs of
  different sockets are synchronized. KRD profits from this feature to perform inter-socket analysis. One
  needs to take care however that the clocks do not desynchronize after boot. Clocks can become desynchronized
  over time due to clock drift or a privileged process may change the value of the TSC. The easiest way to
  detect synchronization problems is that the toolchain will provide bogus results. More information on the
  operation of the TSC can be found in section 17.13 of the Intel64 and IA-32 Architecture Software Developers
  Manual (Volume 3B). In Linux, to check whether the processor has the necessary support for synchronized
  clocks (even when in deep C- states), look for the "nonstop_tsc" flag in /proc/cpuinfo.
```

GCC features:
```
- LOI/KRD makes use of some GCC C extensions to specify alignments or identify unused variables. Some of these
  features may be supported by other compilers. 
```

4.  Compiling an application with LOI/KRD
-----------------------------------------

See examples/TUTORIAL


5.  Using the LOI and KRD toolchain
-----------------------------------

See examples/TUTORIAL


6.  Acknowledgement
-------------------

Many people have influenced the development of LOI and KRD.  I would like to thank the Matsuoka
Laboratory at TokyoTech first of all for hosting me for two years,  for providing tons of resources
for testing and experimentation and for the many fruitful discussions with its members. I also want
to thank Prof. Taura of University of Tokyo for long discussions on performance analysis or
task-parallel runtimes. 

---
Miquel Pericàs, Chalmers University of Technology
Göteborg, July 27th 2014

